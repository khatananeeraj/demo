How to enable HTTPS in a Spring Boot Java application

1.	Get an SSL certificate
a.	Generate a self-signed SSL certificate
Open your Terminal prompt and write the following command to create a PKCS12 keystore:
keytool -genkeypair -alias springbootdemo -storetype PKCS12 -keyalg RSA -keysize 2048 -validity 365 -keystore springbootdemo.p12
You then will be asked to enter a password for the keystore. It must have at least 6 characters.
Finally, you will be asked to input some information, but you are free to skip all of it (just press Return to skip an option).
At the end of this operation, we’ll get a keystore containing a brand new SSL certificate.
b.	Use an existing SSL certificate
If you have already got an SSL certificate, you can import it into a keystore and use it to enable HTTPS in a Spring Boot application.
Using the following command, you’ll create a new keystore containing your certificate.
keytool -import -alias springbootdemo -file myCertificate.crt -keystore springbootdemo.p12 -storepass password
2.	Enable HTTPS in Spring Boot
We can now set up Spring Boot to accept requests over HTTPS instead of HTTP by using that certificate.
The first thing to do is placing the keystore file inside the Spring Boot project. You may want to put it in the resources folder or the root folder.
All you need to do is open up your application.properties file, define the following properties:
server.port = 8443
server.ssl.key-alias = springbootdemo
server.ssl.key-store-type = PKCS12
server.ssl.key-store-password = password
server.ssl.key-store = classpath:springbootdemo.p12
3.	Redirect HTTP requests to HTTPS
Now we may want to redirect all traffic to HTTPS. Here we create a Connector class and add in the Tomcat Configuration programmatically.
4.	Import Root CA Certificate into web browser.
We can enable trust in our web browser for our private certificate authority.
We will use the chrome browser here to demonstrate this.
Just open the settings in chrome, expand the “Advanced” section and then go to “Manage certificates“.
Here you import the root ca certificate into the browser as new authority.
Mark the first checkbox stated “Trust this website for identifying websites”.
